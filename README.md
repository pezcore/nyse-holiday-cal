# Overview

Library crate for looking up market holiday information as observed by the [New
York Stock Exchange](https://en.wikipedia.org/wiki/New_York_Stock_Exchange).

## Use-cases
- Determine if a specific date is/was an NYSE market holiday
- Determine which holiday NYSE observed or plans to observe on a specific date
- Iterate over sequences of consecutive business days
- Perform basic business-day arithmetic on dates
- Convert between dates and business day of month ordinals

## Usage
This crate is designed to work with foreign date types which represent dates in
the [proleptic Gregorian
calendar](https://en.wikipedia.org/wiki/Proleptic_Gregorian_calendar). By
default, [`chrono::NaiveDate`] is used as the date type, but with optional crate
feature `time` enabled, support for [`time::Date`] is added as well.

The main interface for looking up holiday and business day information is trait
[`HolidayCal`]. This trait is implemented for all supported foreign date types.

The main interface for performing business-day arithmetic is struct
[`DeltaBusinessDays`]. This struct represents an integer number of business days
and can be used in arithmetic with supported foreign date types via the `Add`
and `Sub` traits from the standard library.

## Why not other crates?

There are several other holiday calendar crates in the Rust ecosystem, which
provide a framework for developing holiday calendar functionality:

- [`bdays`](https://docs.rs/bdays/latest/bdays/)
- [`cal-calc`](https://docs.rs/cal-calc/0.2.0/cal_calc/index.html)
- [`holiday`](https://docs.rs/holiday/0.1.2/holiday/index.html)

However, these crates are ill-suited for implementing NYSE holiday calendars
because they all assume that the holiday status of **any** date can be
determined. In general, it is only possible to determine if a date is an NYSE
market holiday if it is before 31 December of the year of the latest published
official NYSE holiday calendar.

## Design

The design of the this create is simple, it maintains a list of observed and
planned market holidays according to NYSE's officially released schedule. This
list of known market holidays defines a range spanning from 1 Jan in the year
[`MIN_YEAR`] to 31 December in the year [`MAX_YEAR`]. Dates that fall in this
range are considered market holidays if and only if they are equal to one of the
dates in the holiday list. For dates that fall outside this range, it is not
possible to determine whether or not they are NYSE market holidays and so
generally, `Err(OutOfRange)` is returned for queries involving such dates.

When the NYSE publishes their market holiday calendar for a new year, the
holidays list will be updated accordingly, [`MAX_YEAR`] will be incremented
accordingly, and the patch version of this crate will be incremented.

Typically, NYSE publishes holiday calendars 1-2 years in advance of the current
date.
