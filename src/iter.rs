use chrono::NaiveDate;
use std::iter::FusedIterator;

#[cfg(feature = "time")]
use time::Date;

use crate::HolidayCal;

#[derive(Debug)]
pub struct DaysIter<T>(pub(crate) T);

/// An reversable iterator over all business days.
///
/// This struct is only constructed via [`HolidayCal::busday_iter`]. This iterator sequentially
/// yields all business day dates occuring after the date from which it was constructed, or if it
/// is reversed, before the date from which it was constructed.
///
/// ```
/// use chrono::NaiveDate;
/// use nyse_holiday_cal::HolidayCal;
///
/// let start_date = NaiveDate::from_ymd_opt(2023, 04, 05).unwrap();
///
/// // Yielding Business days sequentially going forward
/// let mut iterator = start_date.busday_iter();
///
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(2023, 04, 06).unwrap()));
/// // Note Good Friday is skipped
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(2023, 04, 10).unwrap()));
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(2023, 04, 11).unwrap()));
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(2023, 04, 12).unwrap()));
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(2023, 04, 13).unwrap()));
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(2023, 04, 14).unwrap()));
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(2023, 04, 17).unwrap()));
///
/// // Yielding business days sequentially going backward
/// let mut iterator = start_date.busday_iter().rev();
///
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(2023, 04, 04).unwrap()));
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(2023, 04, 03).unwrap()));
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(2023, 03, 31).unwrap()));
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(2023, 03, 30).unwrap()));
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(2023, 03, 29).unwrap()));
/// ```
///
/// The iterator becomes exhausted when it reaches the end of the calendar:
///
/// ```
/// use chrono::NaiveDate;
/// use nyse_holiday_cal::{HolidayCal, MAX_YEAR};
///
/// let max_year = MAX_YEAR.into();
/// let start_date = NaiveDate::from_ymd_opt(max_year, 12, 27).unwrap();
///
/// let mut iterator = start_date.busday_iter();
///
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(max_year, 12, 28).unwrap()));
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(max_year, 12, 29).unwrap()));
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(max_year, 12, 30).unwrap()));
/// assert_eq!(iterator.next(), Some(NaiveDate::from_ymd_opt(max_year, 12, 31).unwrap()));
/// assert_eq!(iterator.next(), None);
/// assert_eq!(iterator.next(), None);
///
/// ```
pub struct BusDaysIter<T>(pub(crate) T);

impl Iterator for DaysIter<NaiveDate> {
    type Item = NaiveDate;
    fn next(&mut self) -> Option<NaiveDate> {
        self.0 = self.0.succ_opt()?;
        Some(self.0)
    }
}

impl DoubleEndedIterator for DaysIter<NaiveDate> {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.0 = self.0.pred_opt()?;
        Some(self.0)
    }
}

impl<T> FusedIterator for DaysIter<T> where DaysIter<T>: Iterator {}

#[cfg(feature = "time")]
impl Iterator for DaysIter<Date> {
    type Item = Date;
    fn next(&mut self) -> Option<Date> {
        self.0 = self.0.next_day()?;
        Some(self.0)
    }
}

#[cfg(feature = "time")]
impl DoubleEndedIterator for DaysIter<Date> {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.0 = self.0.previous_day()?;
        Some(self.0)
    }
}

impl<T> Iterator for BusDaysIter<T>
where
    T: HolidayCal + Copy,
    DaysIter<T>: Iterator<Item = T>,
{
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        self.0 = DaysIter(self.0)
            .take_while(|&date| date.is_in_range())
            .find(|&date| date.is_busday() == Ok(true))?;
        Some(self.0)
    }
}

impl<T> DoubleEndedIterator for BusDaysIter<T>
where
    T: HolidayCal + Copy,
    DaysIter<T>: DoubleEndedIterator<Item = T>,
{
    fn next_back(&mut self) -> Option<Self::Item> {
        self.0 = DaysIter(self.0)
            .rev()
            .take_while(|&date| date.is_in_range())
            .find(|&date| date.is_busday() == Ok(true))?;
        Some(self.0)
    }
}

impl<T> FusedIterator for BusDaysIter<T> where BusDaysIter<T>: Iterator {}

#[cfg(test)]
mod tests {
    use crate::{HolidayCal, MIN_YEAR};
    use chrono::NaiveDate;
    use lazy_static::lazy_static;

    lazy_static! {
        #[rustfmt::skip]
        static ref EXPECTED: [NaiveDate; 16] = [
            NaiveDate::from_ymd_opt(2023, 4,  6).unwrap(),
            NaiveDate::from_ymd_opt(2023, 4, 10).unwrap(),
            NaiveDate::from_ymd_opt(2023, 4, 11).unwrap(),
            NaiveDate::from_ymd_opt(2023, 4, 12).unwrap(),
            NaiveDate::from_ymd_opt(2023, 4, 13).unwrap(),
            NaiveDate::from_ymd_opt(2023, 4, 14).unwrap(),
            NaiveDate::from_ymd_opt(2023, 4, 17).unwrap(),
            NaiveDate::from_ymd_opt(2023, 4, 18).unwrap(),
            NaiveDate::from_ymd_opt(2023, 4, 19).unwrap(),
            NaiveDate::from_ymd_opt(2023, 4, 20).unwrap(),
            NaiveDate::from_ymd_opt(2023, 4, 21).unwrap(),
            NaiveDate::from_ymd_opt(2023, 4, 24).unwrap(),
            NaiveDate::from_ymd_opt(2023, 4, 25).unwrap(),
            NaiveDate::from_ymd_opt(2023, 4, 26).unwrap(),
            NaiveDate::from_ymd_opt(2023, 4, 27).unwrap(),
            NaiveDate::from_ymd_opt(2023, 4, 28).unwrap(),
        ];
    }

    #[test]
    fn busday_iter_forward() {
        let start = NaiveDate::from_ymd_opt(2023, 4, 5).unwrap();
        let ymd = |y, m, d| NaiveDate::from_ymd_opt(y, m, d).unwrap();

        let mut iterator = start.busday_iter();
        #[rustfmt::skip]
        assert_eq!(iterator.next(), Some(ymd(2023, 4,  6)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 10)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 11)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 12)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 13)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 14)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 17)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 18)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 19)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 20)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 21)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 24)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 25)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 26)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 27)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 28)));
    }

    #[test]
    fn busday_iter_backwards() {
        let start = NaiveDate::from_ymd_opt(2023, 4, 29).unwrap();
        let ymd = |y, m, d| NaiveDate::from_ymd_opt(y, m, d).unwrap();
        let mut iterator = start.busday_iter().rev();

        assert_eq!(iterator.next(), Some(ymd(2023, 4, 28)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 27)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 26)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 25)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 24)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 21)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 20)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 19)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 18)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 17)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 14)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 13)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 12)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 11)));
        assert_eq!(iterator.next(), Some(ymd(2023, 4, 10)));
        #[rustfmt::skip]
        assert_eq!(iterator.next(), Some(ymd(2023, 4,  6)));
    }

    #[test]
    fn no_weekends_or_holidays() {
        let start = NaiveDate::from_ymd_opt(MIN_YEAR.into(), 1, 1).unwrap();
        for date in start.busday_iter() {
            assert!(!date.is_weekend());
            assert!(!date.is_holiday().unwrap());
        }
    }
}
