use std::{collections::HashMap, ops::RangeInclusive};

use chrono::{Datelike, NaiveDate};
use lazy_static::lazy_static;

use crate::{Holiday, HolidayCal, MAX_YEAR, MIN_YEAR};

lazy_static! {
    pub static ref CALENDAR: HashMap<NaiveDate, Holiday> = {
        use Holiday::*;

        #[rustfmt::skip]
        let calendar = [
            (NaiveDate::from_ymd_opt(2020,  1,  1).unwrap(), NewYearsDay),
            (NaiveDate::from_ymd_opt(2020,  1, 20).unwrap(), MartinLutherKingJrDay),
            (NaiveDate::from_ymd_opt(2020,  2, 17).unwrap(), WashingtonsBirthday),
            (NaiveDate::from_ymd_opt(2020,  4, 10).unwrap(), GoodFriday),
            (NaiveDate::from_ymd_opt(2020,  5, 25).unwrap(), MemorialDay),
            // Juneteenth not observed before '22'.unwrap()
            (NaiveDate::from_ymd_opt(2020,  7,  3).unwrap(), IndependenceDay),
            (NaiveDate::from_ymd_opt(2020,  9,  7).unwrap(), LaborDay),
            (NaiveDate::from_ymd_opt(2020, 11, 26).unwrap(), ThanksgivingDay),
            (NaiveDate::from_ymd_opt(2020, 12, 25).unwrap(), ChristmasDay),
            (NaiveDate::from_ymd_opt(2021,  1,  1).unwrap(), NewYearsDay),
            (NaiveDate::from_ymd_opt(2021,  1, 18).unwrap(), MartinLutherKingJrDay),
            (NaiveDate::from_ymd_opt(2021,  2, 15).unwrap(), WashingtonsBirthday),
            (NaiveDate::from_ymd_opt(2021,  4,  2).unwrap(), GoodFriday),
            (NaiveDate::from_ymd_opt(2021,  5, 31).unwrap(), MemorialDay),
            // Juneteenth not observed before '22'.unwrap()
            (NaiveDate::from_ymd_opt(2021,  7,  5).unwrap(), IndependenceDay),
            (NaiveDate::from_ymd_opt(2021,  9,  6).unwrap(), LaborDay),
            (NaiveDate::from_ymd_opt(2021, 11, 25).unwrap(), ThanksgivingDay),
            (NaiveDate::from_ymd_opt(2021, 12, 24).unwrap(), ChristmasDay),
            // New Year's Day not observed in '22 .unwrap()
            (NaiveDate::from_ymd_opt(2022,  1, 17).unwrap(), MartinLutherKingJrDay),
            (NaiveDate::from_ymd_opt(2022,  2, 21).unwrap(), WashingtonsBirthday),
            (NaiveDate::from_ymd_opt(2022,  4, 15).unwrap(), GoodFriday),
            (NaiveDate::from_ymd_opt(2022,  5, 30).unwrap(), MemorialDay),
            (NaiveDate::from_ymd_opt(2022,  6, 20).unwrap(), Juneteenth),
            (NaiveDate::from_ymd_opt(2022,  7,  4).unwrap(), IndependenceDay),
            (NaiveDate::from_ymd_opt(2022,  9,  5).unwrap(), LaborDay),
            (NaiveDate::from_ymd_opt(2022, 11, 24).unwrap(), ThanksgivingDay),
            (NaiveDate::from_ymd_opt(2022, 12, 26).unwrap(), ChristmasDay),
            (NaiveDate::from_ymd_opt(2023,  1,  2).unwrap(), NewYearsDay),
            (NaiveDate::from_ymd_opt(2023,  1, 16).unwrap(), MartinLutherKingJrDay),
            (NaiveDate::from_ymd_opt(2023,  2, 20).unwrap(), WashingtonsBirthday),
            (NaiveDate::from_ymd_opt(2023,  4,  7).unwrap(), GoodFriday),
            (NaiveDate::from_ymd_opt(2023,  5, 29).unwrap(), MemorialDay),
            (NaiveDate::from_ymd_opt(2023,  6, 19).unwrap(), Juneteenth),
            (NaiveDate::from_ymd_opt(2023,  7,  4).unwrap(), IndependenceDay),
            (NaiveDate::from_ymd_opt(2023,  9,  4).unwrap(), LaborDay),
            (NaiveDate::from_ymd_opt(2023, 11, 23).unwrap(), ThanksgivingDay),
            (NaiveDate::from_ymd_opt(2023, 12, 25).unwrap(), ChristmasDay),
            (NaiveDate::from_ymd_opt(2024,  1,  1).unwrap(), NewYearsDay),
            (NaiveDate::from_ymd_opt(2024,  1, 15).unwrap(), MartinLutherKingJrDay),
            (NaiveDate::from_ymd_opt(2024,  2, 19).unwrap(), WashingtonsBirthday),
            (NaiveDate::from_ymd_opt(2024,  3, 29).unwrap(), GoodFriday),
            (NaiveDate::from_ymd_opt(2024,  5, 27).unwrap(), MemorialDay),
            (NaiveDate::from_ymd_opt(2024,  6, 19).unwrap(), Juneteenth),
            (NaiveDate::from_ymd_opt(2024,  7,  4).unwrap(), IndependenceDay),
            (NaiveDate::from_ymd_opt(2024,  9,  2).unwrap(), LaborDay),
            (NaiveDate::from_ymd_opt(2024, 11, 28).unwrap(), ThanksgivingDay),
            (NaiveDate::from_ymd_opt(2024, 12, 25).unwrap(), ChristmasDay),
            (NaiveDate::from_ymd_opt(2025,  1,  1).unwrap(), NewYearsDay),
            (NaiveDate::from_ymd_opt(2025,  1, 20).unwrap(), MartinLutherKingJrDay),
            (NaiveDate::from_ymd_opt(2025,  2, 17).unwrap(), WashingtonsBirthday),
            (NaiveDate::from_ymd_opt(2025,  4, 18).unwrap(), GoodFriday),
            (NaiveDate::from_ymd_opt(2025,  5, 26).unwrap(), MemorialDay),
            (NaiveDate::from_ymd_opt(2025,  6, 19).unwrap(), Juneteenth),
            (NaiveDate::from_ymd_opt(2025,  7,  4).unwrap(), IndependenceDay),
            (NaiveDate::from_ymd_opt(2025,  9,  1).unwrap(), LaborDay),
            (NaiveDate::from_ymd_opt(2025, 11, 27).unwrap(), ThanksgivingDay),
            (NaiveDate::from_ymd_opt(2025, 12, 25).unwrap(), ChristmasDay),
            (NaiveDate::from_ymd_opt(2026,  1,  1).unwrap(), NewYearsDay),
            (NaiveDate::from_ymd_opt(2026,  1, 19).unwrap(), MartinLutherKingJrDay),
            (NaiveDate::from_ymd_opt(2026,  2, 16).unwrap(), WashingtonsBirthday),
            (NaiveDate::from_ymd_opt(2026,  4,  3).unwrap(), GoodFriday),
            (NaiveDate::from_ymd_opt(2026,  5, 25).unwrap(), MemorialDay),
            (NaiveDate::from_ymd_opt(2026,  6, 19).unwrap(), Juneteenth),
            (NaiveDate::from_ymd_opt(2026,  7,  3).unwrap(), IndependenceDay),
            (NaiveDate::from_ymd_opt(2026,  9,  7).unwrap(), LaborDay),
            (NaiveDate::from_ymd_opt(2026, 11, 26).unwrap(), ThanksgivingDay),
            (NaiveDate::from_ymd_opt(2026, 12, 25).unwrap(), ChristmasDay),
            (NaiveDate::from_ymd_opt(2027,  1,  1).unwrap(), NewYearsDay),
            (NaiveDate::from_ymd_opt(2027,  1, 18).unwrap(), MartinLutherKingJrDay),
            (NaiveDate::from_ymd_opt(2027,  2, 15).unwrap(), WashingtonsBirthday),
            (NaiveDate::from_ymd_opt(2027,  3, 26).unwrap(), GoodFriday),
            (NaiveDate::from_ymd_opt(2027,  5, 31).unwrap(), MemorialDay),
            (NaiveDate::from_ymd_opt(2027,  6, 18).unwrap(), Juneteenth),
            (NaiveDate::from_ymd_opt(2027,  7,  5).unwrap(), IndependenceDay),
            (NaiveDate::from_ymd_opt(2027,  9,  6).unwrap(), LaborDay),
            (NaiveDate::from_ymd_opt(2027, 11, 25).unwrap(), ThanksgivingDay),
            (NaiveDate::from_ymd_opt(2027, 12, 24).unwrap(), ChristmasDay),
        ];
        HashMap::from(calendar)
    };

    pub static ref CAL2BUS: HashMap<NaiveDate, u8> = {
        let mut map = HashMap::with_capacity(2000);
        let start = NaiveDate::from_ymd_opt(MIN_YEAR.into(), 1, 1).unwrap();
        let end = NaiveDate::from_ymd_opt(MAX_YEAR.into(), 12, 31).unwrap();
        let mut n = 1;
        for date in start.iter_days().take_while(|&x| x <= end) {
            if date.day() == 1 { n = 1; }
            if date.is_busday().unwrap() {
                map.insert(date, n);
                n += 1;
            }
        }
        map
    };

    pub static ref BUS2CAL: HashMap<(u16, u8, u8), u8> = {
        let mut map = HashMap::with_capacity(2000);
        let start = NaiveDate::from_ymd_opt(MIN_YEAR.into(), 1, 1).unwrap();
        let end = NaiveDate::from_ymd_opt(MAX_YEAR.into(), 12, 31).unwrap();
        let mut n = 1;
        for date in start.iter_days().take_while(|&x| x <= end) {
            if date.day() == 1 { n = 1; }
            if date.is_busday().unwrap() {
                let y = date.year().try_into().unwrap();
                let m = date.month().try_into().unwrap();
                let d = date.day().try_into().unwrap();
                map.insert((y, m, n), d);
                n += 1;
            }
        }
        map
    };

    /// First and last Business day of each calendar month
    pub static ref MONTH_BUS_RANGE: HashMap<(u16, u8), RangeInclusive<u8>> = {
        #[rustfmt::skip]
        let data = [
            ((2020,  1), 2..=31),
            ((2020,  2), 3..=28),
            ((2020,  3), 2..=31),
            ((2020,  4), 1..=30),
            ((2020,  5), 1..=29),
            ((2020,  6), 1..=30),
            ((2020,  7), 1..=31),
            ((2020,  8), 3..=31),
            ((2020,  9), 1..=30),
            ((2020, 10), 1..=30),
            ((2020, 11), 2..=30),
            ((2020, 12), 1..=31),
            //
            ((2021,  1), 4..=29),
            ((2021,  2), 1..=26),
            ((2021,  3), 1..=31),
            ((2021,  4), 1..=30),
            ((2021,  5), 3..=28),
            ((2021,  6), 1..=30),
            ((2021,  7), 1..=30),
            ((2021,  8), 2..=31),
            ((2021,  9), 1..=30),
            ((2021, 10), 1..=29),
            ((2021, 11), 1..=30),
            ((2021, 12), 1..=31),
            //
            ((2022,  1), 3..=31),
            ((2022,  2), 1..=28),
            ((2022,  3), 1..=31),
            ((2022,  4), 1..=29),
            ((2022,  5), 2..=31),
            ((2022,  6), 1..=30),
            ((2022,  7), 1..=29),
            ((2022,  8), 1..=31),
            ((2022,  9), 1..=30),
            ((2022, 10), 3..=31),
            ((2022, 11), 1..=30),
            ((2022, 12), 1..=30),
            //
            ((2023,  1), 3..=31),
            ((2023,  2), 1..=28),
            ((2023,  3), 1..=31),
            ((2023,  4), 3..=28),
            ((2023,  5), 1..=31),
            ((2023,  6), 1..=30),
            ((2023,  7), 3..=31),
            ((2023,  8), 1..=31),
            ((2023,  9), 1..=29),
            ((2023, 10), 2..=31),
            ((2023, 11), 1..=30),
            ((2023, 12), 1..=29),
            //
            ((2024,  1), 2..=31),
            ((2024,  2), 1..=29),
            ((2024,  3), 1..=29),
            ((2024,  4), 1..=30),
            ((2024,  5), 1..=31),
            ((2024,  6), 3..=28),
            ((2024,  7), 1..=31),
            ((2024,  8), 1..=30),
            ((2024,  9), 3..=30),
            ((2024, 10), 1..=31),
            ((2024, 11), 1..=29),
            ((2024, 12), 2..=31),
            //
            ((2025,  1), 2..=31),
            ((2025,  2), 3..=28),
            ((2025,  3), 3..=31),
            ((2025,  4), 1..=30),
            ((2025,  5), 1..=30),
            ((2025,  6), 2..=30),
            ((2025,  7), 1..=31),
            ((2025,  8), 1..=29),
            ((2025,  9), 2..=30),
            ((2025, 10), 1..=31),
            ((2025, 11), 3..=28),
            ((2025, 12), 1..=31),
            //
            ((2026,  1), 2..=30),
            ((2026,  2), 2..=27),
            ((2026,  3), 2..=31),
            ((2026,  4), 1..=30),
            ((2026,  5), 1..=29),
            ((2026,  6), 1..=30),
            ((2026,  7), 1..=31),
            ((2026,  8), 3..=31),
            ((2026,  9), 1..=30),
            ((2026, 10), 1..=30),
            ((2026, 11), 2..=30),
            ((2026, 12), 1..=31),
            //
            ((2027,  1), 4..=29),
            ((2027,  2), 1..=26),
            ((2027,  3), 1..=31),
            ((2027,  4), 1..=30),
            ((2027,  5), 3..=28),
            ((2027,  6), 1..=30),
            ((2027,  7), 1..=30),
            ((2027,  8), 2..=31),
            ((2027,  9), 1..=30),
            ((2027, 10), 1..=29),
            ((2027, 11), 1..=30),
            ((2027, 12), 1..=31),
        ];
        HashMap::from(data)
    };
}
