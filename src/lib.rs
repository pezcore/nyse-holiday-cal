#![cfg_attr(docsrs, feature(doc_auto_cfg))]
#![doc = include_str!("../README.md")]

use chrono::{Datelike, NaiveDate, Weekday};

mod arithmetic;
pub use arithmetic::DeltaBusinessDays;

mod iter;
mod luts;
use luts::{CAL2BUS, CALENDAR};

pub use iter::BusDaysIter;

pub type Result<T> = std::result::Result<T, OutOfRange>;

/// Holidays observed by the NYSE
#[non_exhaustive]
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Holiday {
    NewYearsDay,
    MartinLutherKingJrDay,
    WashingtonsBirthday,
    GoodFriday,
    MemorialDay,
    Juneteenth,
    IndependenceDay,
    LaborDay,
    ThanksgivingDay,
    ChristmasDay,
}

/// Error type indicating that a date was outside the range of the existing NYSE holiday calendar
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct OutOfRange;

/// Determine NYSE holiday information for dates
pub trait HolidayCal {
    /// Try to determine holiday information for this day.
    ///
    /// If it is not possible to determine whether the day is an NYSE market holiday, return
    /// `Err(OutOfRange)`. This will be the case if the date falls before [`MIN_YEAR`] or after
    /// [`MAX_YEAR`]. Otherwise return an `Option<Holiday>` describing which holiday it is:
    ///
    /// | Input | Output |
    /// | ----- | ------ |
    /// | Holiday status indefinite | `Err(OutOfRange)` |
    /// | Definite holiday | `Ok(Some(x))` where `x` identifies which holiday it is
    /// | Definite non-holiday | `Ok(None)` |
    ///
    /// Note: Weekends are **not** considered NYSE holidays.
    ///
    /// ## Examples
    /// ```
    /// use nyse_holiday_cal::{HolidayCal, OutOfRange, Holiday::*};
    /// use chrono::NaiveDate;
    ///
    /// // Holidays
    /// assert_eq!(NaiveDate::from_ymd_opt(2023, 4,  7).unwrap().holiday(), Ok(Some(GoodFriday)));
    /// assert_eq!(NaiveDate::from_ymd_opt(2023, 7,  4).unwrap().holiday(), Ok(Some(IndependenceDay)));
    /// assert_eq!(NaiveDate::from_ymd_opt(2024, 1,  1).unwrap().holiday(), Ok(Some(NewYearsDay)));
    /// assert_eq!(NaiveDate::from_ymd_opt(2025, 5, 26).unwrap().holiday(), Ok(Some(MemorialDay)));
    ///
    /// // Non-holidays
    /// assert_eq!(NaiveDate::from_ymd_opt(2023, 4, 20).unwrap().holiday(), Ok(None));
    /// assert_eq!(NaiveDate::from_ymd_opt(2025, 5,  1).unwrap().holiday(), Ok(None));
    ///
    ///
    /// // Out of range dates return Err
    /// assert_eq!(NaiveDate::from_ymd_opt(2030, 1, 1).unwrap().holiday(), Err(OutOfRange));
    /// assert_eq!(NaiveDate::from_ymd_opt(2016, 1, 1).unwrap().holiday(), Err(OutOfRange));
    /// ```
    fn holiday(&self) -> Result<Option<Holiday>>;

    /// Determine if `self` is in range of the existing holiday calendar. In general, it is not
    /// possible to determine if a date is an NYSE holiday if it is not in range.
    fn is_in_range(&self) -> bool;

    /// Determine if `self` is a weekend
    fn is_weekend(&self) -> bool;

    /// Get business day ordinal of a specific calendar date
    ///
    /// If `self` is a business day, return `Some(n)` such that `self` is the `n`<sup>th</sup>
    /// business day of the calendar month containing `self`, otherwise return `None`
    fn cal2bus(&self) -> Option<u8>;

    /// Get the day-of-month of the last business day of the calendar month containing `self`,
    /// or, if the date is out of range, return `Err(OutOfRange)`
    fn bus_month_end(&self) -> Result<u8>;

    /// Get the day-of-month of the first business day of the calendar month containing `self`,
    /// or, if the date is out of range, return `Err(OutOfRange)`
    fn bus_month_begin(&self) -> Result<u8>;

    /// Determine if `self` is an NYSE holiday
    fn is_holiday(&self) -> Result<bool> {
        Ok(self.holiday()?.is_some())
    }

    /// Determine if the NYSE is open for trading on a certain day.
    ///
    /// When it is possible to determine with certainty whether the NYSE is open for trading on
    /// `self`, return `Ok(b)` with the inner value indicating whether the date is an open market
    /// day. When it is not possible to determine, return `Err(OutOfRange)`. This is similar to
    /// [`Self::is_holiday`] except that it accounts for weekends as non-business days.
    ///
    /// Note: **all** weekends are considered definite non-business days even if they are outside
    /// the range of the calendar because the NYSE will never trade on a weekend.
    fn is_busday(&self) -> Result<bool> {
        Ok(!(self.is_weekend() || self.is_holiday()?))
    }

    /// Return an iterator of business days occuring after `self`. If reversed, it becomes an
    /// iterator over business days occuring before `self`. See the documentation for the return
    /// type [`BusDaysIter`] for details
    fn busday_iter(&self) -> crate::iter::BusDaysIter<Self>
    where
        iter::BusDaysIter<Self>: DoubleEndedIterator<Item = Self>,
        Self: Sized + Copy,
    {
        iter::BusDaysIter(*self)
    }

    /// Get the calendar day of a specific business day
    ///
    /// Return the calendar day (i.e. day-of-month) which is the `n`<sup>th</sup> business day of
    /// the `m`<sup>th</sup> month of year `y`.
    fn bus2cal(y: u16, m: u8, n: u8) -> Result<u8> {
        luts::BUS2CAL.get(&(y, m, n)).copied().ok_or(OutOfRange)
    }
}

/// Beginning of NYSE holiday calendar.
///
/// It is not possible to determine if a date is an NYSE market holiday if it is before 1 Jan of
/// this year
pub const MIN_YEAR: u16 = 2020;
/// End of NYSE holiday calendar.
///
/// It is not possible to determine if a date is an NYSE market holiday if it is after 31 Dec of
/// this year
pub const MAX_YEAR: u16 = 2027;

const RANGE: std::ops::RangeInclusive<u16> = MIN_YEAR..=MAX_YEAR;

impl HolidayCal for NaiveDate {
    fn holiday(&self) -> Result<Option<Holiday>> {
        if !self.is_in_range() {
            return Err(OutOfRange);
        }
        Ok(CALENDAR.get(self).copied())
    }

    fn is_in_range(&self) -> bool {
        self.year()
            .try_into()
            .is_ok_and(|x: u16| RANGE.contains(&x))
    }

    fn is_weekend(&self) -> bool {
        use Weekday::*;
        self.weekday() == Sat || self.weekday() == Sun
    }

    fn cal2bus(&self) -> Option<u8> {
        CAL2BUS.get(self).copied()
    }

    fn bus_month_end(&self) -> Result<u8> {
        let month = (
            self.year().try_into().unwrap(),
            self.month().try_into().unwrap(),
        );
        luts::MONTH_BUS_RANGE
            .get(&month)
            .map(|x| *x.end())
            .ok_or(OutOfRange)
    }

    fn bus_month_begin(&self) -> Result<u8> {
        let month = (
            self.year().try_into().unwrap(),
            self.month().try_into().unwrap(),
        );
        luts::MONTH_BUS_RANGE
            .get(&month)
            .map(|x| *x.start())
            .ok_or(OutOfRange)
    }
}

#[cfg(feature = "time")]
impl HolidayCal for time::Date {
    fn holiday(&self) -> Result<Option<Holiday>> {
        if !self.is_in_range() {
            return Err(OutOfRange);
        }
        let (y, d) = (self.year(), self.day().into());
        let m: u8 = self.month().into();
        let naive_date = NaiveDate::from_ymd_opt(y, m.into(), d).unwrap();
        Ok(CALENDAR.get(&naive_date).copied())
    }
    fn is_in_range(&self) -> bool {
        self.year()
            .try_into()
            .map_or(false, |x: u16| RANGE.contains(&x))
    }
    fn is_weekend(&self) -> bool {
        use time::Weekday::*;
        self.weekday() == Saturday || self.weekday() == Sunday
    }

    fn cal2bus(&self) -> Option<u8> {
        let (y, d) = (self.year(), self.day().into());
        let m: u8 = self.month().into();
        let naive_date = NaiveDate::from_ymd_opt(y, m.into(), d).unwrap();
        CAL2BUS.get(&naive_date).copied()
    }

    fn bus_month_end(&self) -> Result<u8> {
        let month = (self.year().try_into().unwrap(), self.month().into());
        luts::MONTH_BUS_RANGE
            .get(&month)
            .map(|x| *x.end())
            .ok_or(OutOfRange)
    }

    fn bus_month_begin(&self) -> Result<u8> {
        let month = (self.year().try_into().unwrap(), self.month().into());
        luts::MONTH_BUS_RANGE
            .get(&month)
            .map(|x| *x.start())
            .ok_or(OutOfRange)
    }
}

#[cfg(test)]
mod tests {
    use chrono::NaiveDate;
    use lazy_static::lazy_static;

    mod naive_date;

    #[cfg(feature = "time")]
    mod date;

    lazy_static! {
        #[rustfmt::skip]
        static ref WEEKENDS_VECTORS: [NaiveDate; 20] = [
            NaiveDate::from_ymd_opt(2023,  1,  1).unwrap(),
            NaiveDate::from_ymd_opt(2023,  1,  7).unwrap(),
            NaiveDate::from_ymd_opt(2023,  1,  8).unwrap(),
            NaiveDate::from_ymd_opt(2023,  1, 14).unwrap(),
            NaiveDate::from_ymd_opt(2023,  1, 15).unwrap(),
            NaiveDate::from_ymd_opt(2023,  1, 21).unwrap(),
            NaiveDate::from_ymd_opt(2023,  2,  4).unwrap(),
            NaiveDate::from_ymd_opt(2023,  2,  5).unwrap(),
            NaiveDate::from_ymd_opt(2023,  2, 26).unwrap(),
            NaiveDate::from_ymd_opt(2023,  6, 18).unwrap(),
            NaiveDate::from_ymd_opt(2023,  6, 24).unwrap(),
            NaiveDate::from_ymd_opt(2023,  6, 25).unwrap(),
            NaiveDate::from_ymd_opt(2023, 11,  4).unwrap(),
            NaiveDate::from_ymd_opt(2023, 11,  5).unwrap(),
            NaiveDate::from_ymd_opt(2023, 11, 11).unwrap(),
            NaiveDate::from_ymd_opt(2023, 11, 12).unwrap(),
            NaiveDate::from_ymd_opt(2023, 11, 18).unwrap(),
            NaiveDate::from_ymd_opt(2023, 11, 19).unwrap(),
            NaiveDate::from_ymd_opt(2023, 11, 25).unwrap(),
            NaiveDate::from_ymd_opt(2023, 11, 26).unwrap(),
        ];

        #[rustfmt::skip]
        static ref BUSDAYS: [NaiveDate; 15] = [
            NaiveDate::from_ymd_opt(2023,  1,  3).unwrap(),
            NaiveDate::from_ymd_opt(2023,  1,  4).unwrap(),
            NaiveDate::from_ymd_opt(2023,  1,  5).unwrap(),
            NaiveDate::from_ymd_opt(2023,  1,  6).unwrap(),
            NaiveDate::from_ymd_opt(2023,  1,  9).unwrap(),
            NaiveDate::from_ymd_opt(2023,  3,  7).unwrap(),
            NaiveDate::from_ymd_opt(2023,  3, 27).unwrap(),
            NaiveDate::from_ymd_opt(2023,  3, 28).unwrap(),
            NaiveDate::from_ymd_opt(2024,  6,  7).unwrap(),
            NaiveDate::from_ymd_opt(2024,  7,  8).unwrap(),
            NaiveDate::from_ymd_opt(2024,  7,  9).unwrap(),
            NaiveDate::from_ymd_opt(2024,  7, 12).unwrap(),
            NaiveDate::from_ymd_opt(2024, 10, 10).unwrap(),
            NaiveDate::from_ymd_opt(2024, 10, 11).unwrap(),
            NaiveDate::from_ymd_opt(2024, 10, 14).unwrap(), // Columbus Day, not a NYSE holiday
        ];
    }
}
