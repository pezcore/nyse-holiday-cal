use std::ops::{Add, Sub};

use chrono::NaiveDate;

#[cfg(feature = "time")]
use time::Date;

use super::{HolidayCal, OutOfRange};

/// Duration in business days
///
/// This struct is useful for doing basic business-day arithmetic on [`NaiveDate`]:
///
/// ```
/// use nyse_holiday_cal::DeltaBusinessDays;
/// use chrono::NaiveDate;
///
/// let start_date = NaiveDate::from_ymd_opt(2023, 03, 25).unwrap();
///
/// // Business day addition
/// let expected_result = NaiveDate::from_ymd_opt(2023, 05, 15).unwrap();
/// assert_eq!(start_date + DeltaBusinessDays(35), Ok(expected_result));
///
/// // Business day subtraction
/// let expected_result = NaiveDate::from_ymd_opt(2022, 10, 31).unwrap();
/// assert_eq!(start_date - DeltaBusinessDays(100), Ok(expected_result));
/// ```
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct DeltaBusinessDays(pub u64);

impl Add<DeltaBusinessDays> for NaiveDate {
    type Output = Result<Self, OutOfRange>;

    /// Add a number of business days to a [`NaiveDate`]
    fn add(self, rhs: DeltaBusinessDays) -> Self::Output {
        if rhs.0 == 0 {
            return Ok(self);
        }
        self.busday_iter().nth(rhs.0 as usize - 1).ok_or(OutOfRange)
    }
}

impl Sub<DeltaBusinessDays> for NaiveDate {
    type Output = Result<Self, OutOfRange>;

    /// Add a number of business days to a [`NaiveDate`]
    fn sub(self, rhs: DeltaBusinessDays) -> Self::Output {
        if rhs.0 == 0 {
            return Ok(self);
        }
        self.busday_iter()
            .rev()
            .nth(rhs.0 as usize - 1)
            .ok_or(OutOfRange)
    }
}

#[cfg(feature = "time")]
impl Add<DeltaBusinessDays> for Date {
    type Output = Result<Self, OutOfRange>;

    /// Add a number of business days to a [`NaiveDate`]
    fn add(self, rhs: DeltaBusinessDays) -> Self::Output {
        if rhs.0 == 0 {
            return Ok(self);
        }
        self.busday_iter().nth(rhs.0 as usize - 1).ok_or(OutOfRange)
    }
}

#[cfg(feature = "time")]
impl Sub<DeltaBusinessDays> for Date {
    type Output = Result<Self, OutOfRange>;

    /// Add a number of business days to a [`NaiveDate`]
    fn sub(self, rhs: DeltaBusinessDays) -> Self::Output {
        if rhs.0 == 0 {
            return Ok(self);
        }
        self.busday_iter()
            .rev()
            .nth(rhs.0 as usize - 1)
            .ok_or(OutOfRange)
    }
}

#[cfg(test)]
mod tests {
    use super::{DeltaBusinessDays, NaiveDate, OutOfRange};

    #[test]
    fn add_busday() {
        let from_ymd = |y, m, d| NaiveDate::from_ymd_opt(y, m, d).unwrap();

        #[rustfmt::skip]
        let test_vectors: [(NaiveDate, NaiveDate, u64); 11] = [
            (from_ymd(2023,03,25), from_ymd(2023,03,27), 1), // Normal Sat -> Mon
            (from_ymd(2023,08,18), from_ymd(2023,08,21), 1), // Normal Fri -> Mon
            (from_ymd(2023,02,17), from_ymd(2023,02,21), 1), // Fri -> Tue
            (from_ymd(2023,11,22), from_ymd(2023,11,24), 1), // skip ThanksgivingDay only
            (from_ymd(2023,04,06), from_ymd(2023,04,10), 1), // skip GoodFriday and following weekend
            (from_ymd(2023,04,06), from_ymd(2023,04,28), 15), // skip 15 days including a holiday
            (from_ymd(2023,05,26), from_ymd(2023,07,07), 27), // skip multipule holidays
            // 0 should always leave the date unchanged
            (from_ymd(2023,04,01), from_ymd(2023,04,01), 0),
            (from_ymd(2023,08,15), from_ymd(2023,08,15), 0),
            (from_ymd(2023,03,18), from_ymd(2023,03,18), 0),
            (from_ymd(2023,02,28), from_ymd(2023,02,28), 0),
        ];
        for (start, end, duration) in test_vectors {
            assert_eq!(
                start + DeltaBusinessDays(duration),
                Ok(end),
                "Failed to add {} Busdays",
                duration
            );
        }

        let start = NaiveDate::from_ymd_opt(2027, 12, 23).unwrap();
        assert_eq!(start + DeltaBusinessDays(110), Err(OutOfRange));
    }
}
