use crate::*;

mod holiday {
    use crate::*;

    #[test]
    fn holidays() {
        for (ndt, hol) in CALENDAR.iter() {
            assert_eq!(ndt.holiday(), Ok(Some(*hol)));
        }
    }

    #[test]
    fn non_holidays() {
        let start_date = NaiveDate::from_ymd_opt(MIN_YEAR.into(), 01, 01).unwrap();
        let end_date = NaiveDate::from_ymd_opt(MAX_YEAR.into(), 12, 31).unwrap();
        let mut count = 0;
        for date in start_date
            .iter_days()
            .take_while(|&x| x <= end_date)
            .filter(|&x| !CALENDAR.contains_key(&x.try_into().unwrap()))
        {
            assert_eq!(date.holiday(), Ok(None));
            count += 1;
        }
        assert!(count >= 1000); // Sanity check
    }

    #[test]
    fn out_of_range() {
        let start_date = NaiveDate::from_ymd_opt(0, 1, 1).unwrap();
        let end_date = NaiveDate::from_ymd_opt((MIN_YEAR - 1) as _, 12, 31).unwrap();
        for date in start_date.iter_days().take_while(|&x| x <= end_date) {
            assert_eq!(
                date.holiday(),
                Err(OutOfRange),
                "out of date failed for {}",
                date
            );
        }

        let start_date = NaiveDate::from_ymd_opt((MAX_YEAR + 1).into(), 1, 1).unwrap();
        for date in start_date.iter_days().take(10000) {
            assert_eq!(
                date.holiday(),
                Err(OutOfRange),
                "out of date failed for {}",
                date
            );
        }
    }
}

mod is_weekend {
    use crate::*;

    #[test]
    fn weekends() {
        #[rustfmt::skip]
        let test_vec: [NaiveDate; 10] = [
            NaiveDate::from_ymd_opt(2022,  5,  1).unwrap(),
            NaiveDate::from_ymd_opt(2022,  5,  7).unwrap(),
            NaiveDate::from_ymd_opt(2022,  5,  8).unwrap(),
            NaiveDate::from_ymd_opt(2022,  8, 14).unwrap(),
            NaiveDate::from_ymd_opt(2022,  9, 24).unwrap(),
            NaiveDate::from_ymd_opt(2022,  9, 25).unwrap(),
            NaiveDate::from_ymd_opt(2022, 10, 15).unwrap(),
            NaiveDate::from_ymd_opt(2022, 10, 16).unwrap(),
            NaiveDate::from_ymd_opt(2023,  5,  6).unwrap(),
            NaiveDate::from_ymd_opt(2023,  5,  7).unwrap(),
        ];

        for dt in test_vec {
            assert!(dt.is_weekend(), "weekend check failed for {}", dt);
        }
    }
    #[test]
    fn non_weekends() {
        #[rustfmt::skip]
        let test_vec: [NaiveDate; 4] = [
            NaiveDate::from_ymd_opt(2024,  5,  1).unwrap(),
            NaiveDate::from_ymd_opt(2024,  5,  2).unwrap(),
            NaiveDate::from_ymd_opt(2024,  5, 27).unwrap(), // Memorial Day, not weekend
            NaiveDate::from_ymd_opt(2024,  7,  4).unwrap(), // Independence Day, not weekend
        ];

        for dt in test_vec {
            assert!(!dt.is_weekend(), "weekend check failed for {}", dt);
        }
    }
}

mod is_in_range {
    use crate::*;

    #[test]
    fn too_low() {
        let start_date = NaiveDate::from_ymd_opt(0, 1, 1).unwrap();
        let end_date = NaiveDate::from_ymd_opt((MIN_YEAR - 1) as _, 12, 31).unwrap();
        for date in start_date.iter_days().take_while(|&x| x <= end_date) {
            assert!(!date.is_in_range(), "range check failed for {}", date);
        }
    }
    #[test]
    fn too_high() {
        let start_date = NaiveDate::from_ymd_opt((MAX_YEAR + 1).into(), 1, 1).unwrap();
        let end_date = NaiveDate::from_ymd_opt(2160, 12, 31).unwrap();
        for date in start_date.iter_days().take_while(|&x| x <= end_date) {
            assert!(!date.is_in_range(), "range check failed for {}", date);
        }
    }
    #[test]
    fn in_range() {
        let start_date = NaiveDate::from_ymd_opt(MIN_YEAR.into(), 1, 1).unwrap();
        let end_date = NaiveDate::from_ymd_opt(MAX_YEAR.into(), 12, 31).unwrap();
        for date in start_date.iter_days().take_while(|&x| x <= end_date) {
            assert!(date.is_in_range(), "range check failed for {}", date);
        }
    }
}

mod is_holiday {
    use crate::*;

    #[test]
    fn holidays() {
        for (dt, _) in CALENDAR.iter() {
            assert_eq!(dt.is_holiday(), Ok(true));
        }
    }

    #[test]
    fn non_holidays() {
        let start = NaiveDate::from_ymd_opt(MIN_YEAR.into(), 1, 1).unwrap();
        let end = NaiveDate::from_ymd_opt(MAX_YEAR.into(), 12, 31).unwrap();
        for dt in start
            .iter_days()
            .take_while(|&x| x <= end)
            .filter(|x| !CALENDAR.contains_key(x))
        {
            assert_eq!(dt.is_holiday(), Ok(false), "failed on {}", dt);
        }
    }

    #[test]
    fn out_of_range() {
        let start = NaiveDate::from_ymd_opt(1776, 7, 4).unwrap();
        let end = NaiveDate::from_ymd_opt((MIN_YEAR - 1).into(), 12, 31).unwrap();
        for dt in start.iter_days().take_while(|&x| x <= end) {
            assert_eq!(dt.is_holiday(), Err(OutOfRange), "failed on {}", dt);
        }
    }
}

mod is_busday {
    use crate::HolidayCal;

    #[test]
    fn weekends() {
        for dt in *crate::tests::WEEKENDS_VECTORS {
            assert!(dt.is_weekend()); // checks the test itself
            assert_eq!(dt.is_busday(), Ok(false));
        }
    }

    #[test]
    fn holidays() {
        for (dt, _) in crate::CALENDAR.iter() {
            assert_eq!(dt.is_busday(), Ok(false));
        }
    }

    #[test]
    fn business_days() {
        for dt in *crate::tests::BUSDAYS {
            assert_eq!(dt.is_busday(), Ok(true), "Failed on {}", dt);
        }
    }
}

#[test]
fn cal2bus() {
    let d = NaiveDate::from_ymd_opt(2023, 4, 10).unwrap();
    assert_eq!(d.cal2bus(), Some(5));
}
