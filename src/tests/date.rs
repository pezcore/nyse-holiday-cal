use chrono::{Datelike, NaiveDate};

/// Convert a NaiveDate to a time::Date. Panics on all conversion failures.
fn ndt2date(ndt: NaiveDate) -> time::Date {
    let m: u8 = ndt
        .month()
        .try_into()
        .expect("Could not convert month to u8");
    let m = time::Month::try_from(m).expect("Could not convert u8 month to enum Month");
    time::Date::from_calendar_date(ndt.year(), m, ndt.day().try_into().unwrap())
        .expect("Could not convert Date components to time::Date")
}

mod holiday {
    use super::ndt2date;
    use crate::{HolidayCal, CALENDAR};

    #[test]
    fn holidays() {
        for (ndt, hol) in CALENDAR.iter() {
            let date = ndt2date(*ndt);
            assert_eq!(date.holiday(), Ok(Some(*hol)));
        }
    }
}

mod is_busday {
    use super::ndt2date;
    use crate::{HolidayCal, OutOfRange};

    #[test]
    fn weekends() {
        for dt in *crate::tests::WEEKENDS_VECTORS {
            assert!(dt.is_weekend()); // checks the test itself
            assert_eq!(dt.is_busday(), Ok(false));
        }
    }

    #[test]
    fn holidays() {
        for (ndt, _) in crate::CALENDAR.iter() {
            let dt = ndt2date(*ndt);
            assert_eq!(dt.is_busday(), Ok(false));
        }
    }

    #[test]
    fn business_days() {
        for dt in *crate::tests::BUSDAYS {
            assert_eq!(dt.is_busday(), Ok(true), "Failed on {}", dt);
        }
    }

    #[test]
    fn out_of_range() {
        let date = time::Date::from_calendar_date(2010, time::Month::April, 16).unwrap();
        assert_eq!(date.is_busday(), Err(OutOfRange));
    }
}
